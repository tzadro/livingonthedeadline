package com.livingonthedeadline.gameofthroneslocations;

/**
 * The object used to store values for achievementsListView list
 */
public class AchievementItem {
    private String name;
    private String description;
    private Integer id;
    private Boolean userHas;

    public AchievementItem(String name, String description, Integer id, Boolean userHas) {
        this.name = name;
        this.description = description;
        this.id = id;
        this.userHas = userHas;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public Boolean getUserHas() {
        return userHas;
    }
}