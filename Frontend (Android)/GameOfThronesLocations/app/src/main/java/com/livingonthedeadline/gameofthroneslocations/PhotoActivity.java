package com.livingonthedeadline.gameofthroneslocations;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
//TODO add javadoc
/**
 * The activity which offers the user to take a photo, and share it on Facebook afterwards.
 * Also saves the merged photo locally.
 */
public class PhotoActivity extends AppCompatActivity {
    ImageView photoPre = null;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    Bitmap imageBitmap = null;
    Button share = null;
    EditText text = null;
    String urlDownload = null;
    Boolean picTaken = false;
    AlertDialog loading;
    Boolean back = true;

    private FacebookCallback<Sharer.Result> callback = new FacebookCallback<Sharer.Result>() {
        @Override
        public void onSuccess(Sharer.Result loginResult) {
            loading.dismiss();
            back = true;
            AlertDialog alertDialog = new AlertDialog.Builder(PhotoActivity.this).create();
            alertDialog.setTitle("Share:");
            alertDialog.setMessage("Share was successful");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            PhotoActivity.this.finish();
                        }
                    });
            alertDialog.show();
        }

        @Override
        public void onCancel() {
            loading.dismiss();
        }

        @Override
        public void onError(FacebookException error) {
            loading.dismiss();
            AlertDialog alertDialog = new AlertDialog.Builder(PhotoActivity.this).create();
            alertDialog.setTitle("Share:");
            alertDialog.setMessage("Something went wrong. Please try again");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo);
        loading  = new AlertDialog.Builder(PhotoActivity.this).create();
        loading.setCancelable(false);

        photoPre = (ImageView) findViewById(R.id.previewPhoto);
        text = (EditText) findViewById(R.id.description);
        share = (Button) findViewById(R.id.shareButton);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (text.getCurrentTextColor() != Color.BLACK){
                    text.setText("");
                    text.setTextColor(Color.BLACK);
                }
            }
        });
        Bundle bundle = getIntent().getExtras();
        urlDownload="";
        if (bundle.getString("pictureUrl") != null)
            urlDownload = bundle.getString("pictureUrl");

        if (picTaken ==false){
            dispatchTakePictureIntent();
            picTaken = true;
        }

        Button retake = (Button) findViewById(R.id.retake_button);
        retake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });


    }


    private void dispatchTakePictureIntent(){
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(getTempFile(this)));
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
    }

    private File getTempFile(Context context){
        final File path = new File(Environment.getExternalStorageDirectory(), context.getPackageName());
        if(!path.exists())
            path.mkdir();
        return new File(path, "image.tmp");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE){
            if(resultCode == RESULT_OK){
                final File file = getTempFile(this);
                try{
                    imageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), Uri.fromFile(file));
                    ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                    imageBitmap = rotation(exif, imageBitmap);

                    new DownloadMergeSetView(photoPre, imageBitmap).execute(urlDownload);

                }catch (IOException e){
                    e.printStackTrace();
                }
            }
            else if (resultCode == RESULT_CANCELED){
                finish();
            }
        }
    }

    /**
     * rotate Bitmap if source file picture is upside-down, alert user to take landscape picture if portrait photo taken
     * @param exif exif of source picture
     * @param source Bitmap to rotate
     * @return rotated Bitmap
     * @throws IOException
     */
    private  Bitmap rotation(ExifInterface exif, Bitmap source) throws IOException{
        int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        int rotationInDegrees = 0;
        int width = source.getWidth();
        int height = source.getHeight();

        if (rotation == ExifInterface.ORIENTATION_ROTATE_90) {
            rotationInDegrees = 0;
            Toast.makeText(getApplicationContext(), "We recommend taking photos in landscape mode!", Toast.LENGTH_LONG).show();
        }
        else if (rotation == ExifInterface.ORIENTATION_ROTATE_180) {
            rotationInDegrees = 180;
        }
        else if (rotation == ExifInterface.ORIENTATION_ROTATE_270) {
            rotationInDegrees = 0;
            Toast.makeText(getApplicationContext(), "We recommend taking photos in landscape mode!", Toast.LENGTH_LONG).show();
        }
        Matrix matrix = new Matrix();
        if (rotation != 0f) {matrix.preRotate(rotationInDegrees);}

        return Bitmap.createBitmap(source, 0, 0, width,
                height, matrix, true);
    };
    public void shareImage(Bitmap image, String description){
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(image)
                .setCaption(description)
                .build();
        SharePhotoContent shareContent = new SharePhotoContent.Builder().addPhoto(photo).build();
        ShareApi.share(shareContent, callback);
        loading.setTitle("Uploading photo");
        loading.setMessage("Please wait");
        back = false;
        loading.show();
    }

    private  class DownloadMergeSetView extends DownloadToBitmap{
        private Bitmap takenPhoto;
        private ImageView picView;
        private Bitmap downloadPhoto;

        public DownloadMergeSetView(ImageView picView, Bitmap takenPhoto) {
            this.picView = picView;
            this.takenPhoto = takenPhoto;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            downloadPhoto = result;
            MergeBitmaps merge = new MergeBitmaps(downloadPhoto, takenPhoto);
            final Bitmap finalResult = merge.getResult();
            findViewById(R.id.loading).setVisibility(View.GONE);
            picView.setImageBitmap(finalResult);

            share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveImage(finalResult);
                    if (Profile.getCurrentProfile() != null)
                        shareImage(finalResult, text.getText().toString());
                    else{
                        notLoggedIn();
                    }
                }
            });
        }
    }
    private void notLoggedIn(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("You are not logged in!\n Your image is still saved locally")
                .setCancelable(false)
                .setPositiveButton("LOG IN", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Intent intent = new Intent(PhotoActivity.this,LoginActivity.class);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        PhotoActivity.this.finish();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void saveImage(Bitmap bitmap){
        String path = Environment.getExternalStorageDirectory().toString();
        OutputStream fOutputStream = null;
        File dir = new File(path + "/Game Of Thrones Locations/");
        if (!dir.exists()) {
            dir.mkdirs();
        }
        SharedPreferences settings = getSharedPreferences("my_settings", Context.MODE_PRIVATE);
        int n=settings.getInt("picNumber",0);
        n++;
        String picName="GoTL_"+String.valueOf(n)+".jpg";
        File file = new File(path + "/Game Of Thrones Locations/", picName);

        try {
            fOutputStream = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fOutputStream);

            MediaScannerConnection.scanFile(this,
                    new String[]{file.toString()}, null,
                    new MediaScannerConnection.OnScanCompletedListener() {
                        public void onScanCompleted(String path, Uri uri) {
                            Log.i("ExternalStorage", "Scanned " + path + ":");
                            Log.i("ExternalStorage", "-> uri=" + uri);
                        }
                    });

            fOutputStream.flush();
            fOutputStream.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Save Failed", Toast.LENGTH_SHORT).show();
            return;
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK))
            if (back)
                finish();

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        //ignore
    }
}
