package com.livingonthedeadline.gameofthroneslocations;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * A support class for MainActivity which manages the camera movements.
 */
public class BoundsAndZoomCheck implements GoogleMap.OnCameraChangeListener {
    private Context applicationContext;
    private GoogleMap map;
    private CameraPosition lastValidCenter;
    private final LatLngBounds allowedBounds;
    private float maxZoom;

    /**
     * Constructs a BoundsAndZoomCheck object with the neccessary information from the activity.
     * @param applicationContext - the activity context
     * @param map - GoogleMap object
     * @param lastValidCenter - the last map position which was inside the bounds, initially
     *                        set to the center of the bounds
     * @param allowedBounds - object containing the allowed map bounds
     * @param maxZoom - the specified maximum zoom level
     */
    public BoundsAndZoomCheck(Context applicationContext, GoogleMap map, CameraPosition lastValidCenter, LatLngBounds allowedBounds, float maxZoom) {
        this.applicationContext = applicationContext;
        this.map = map;
        this.lastValidCenter = lastValidCenter;
        this.allowedBounds = allowedBounds;
        this.maxZoom = maxZoom;
    }

    /**
     * A method which tracks every camera change, analyses it and executes the necessary changes.
     * @param cameraPosition - the changed camera position
     */
    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (cameraPosition.zoom < maxZoom) {
            zoomBackIn();
        }
        if ((allowedBounds.contains(cameraPosition.target) != true) ) { //&& (noBounds != true)
            moveBackInside();
            Toast.makeText(applicationContext, "No locations there!", Toast.LENGTH_SHORT).show();
        } else {
            lastValidCenter = cameraPosition;
        }
    }

    /**
     * A method which resets the zoom level to the preset value.
     */
    private void zoomBackIn() {
        map.animateCamera(CameraUpdateFactory.zoomTo(maxZoom), 500, new GoogleMap.CancelableCallback() {
            @Override
            public void onFinish() {
                Toast.makeText(applicationContext, "Can't zoom out that far", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onCancel() {
            }
        });
    }

    /**
     * A method which returns the camera inside legal bounds; to the last known valid location.
     */
    private void moveBackInside() {
        map.animateCamera(CameraUpdateFactory.newCameraPosition(lastValidCenter));
    }
}
