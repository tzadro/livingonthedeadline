package com.livingonthedeadline.gameofthroneslocations;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
//TODO add javadoc

/**
 * The activity that displays the user profile and achievement list.
 */
public class ProfileActivity extends AppCompatActivity {
    private static CallbackManager callbackManager;
    ProfileTracker profileTracker;
    TextView nameText;
    ListView achievementsListView;
    Button loginButton;

    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            updateProfile(Profile.getCurrentProfile());
        }

        @Override
        public void onCancel() {
            //ignore
        }

        @Override
        public void onError(FacebookException error) {
            AlertDialog alertDialog = new AlertDialog.Builder(ProfileActivity.this).create();
            alertDialog.setTitle("Something went wrong!");
            alertDialog.setMessage("Please try again");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        final Profile profile = Profile.getCurrentProfile();
        callbackManager = CallbackManager.Factory.create();


        final LoginButton mockLoginButton = new LoginButton(this);

        mockLoginButton.setPublishPermissions("publish_actions");
        mockLoginButton.registerCallback(callbackManager, callback);

        loginButton = (Button) findViewById(R.id.login_button_fb);
        nameText = (TextView) findViewById(R.id.name_text);
        achievementsListView = (ListView) findViewById(R.id.achievements);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mockLoginButton.callOnClick();
            }
        });

        updateProfile(profile);

        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                updateProfile(currentProfile);

            }
        };
        profileTracker.startTracking();

        Button rewardButton = (Button) findViewById(R.id.rewards_button);
        rewardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Profile.getCurrentProfile() != null) {
                    Intent discountsIntent = new Intent(ProfileActivity.this, RewardsActivity.class);
                    startActivity(discountsIntent);
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(ProfileActivity.this).create();
                    alertDialog.setTitle("You can't access rewards without logging in!");
                    alertDialog.setMessage("Please log in to facebook to proceed");
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void updateProfile(Profile profile) {
        ImageView profilePicture = (ImageView) findViewById(R.id.profile_image);
        if (profile != null) {
            loginButton.setBackgroundResource(R.drawable.fb_logout);
            String name = profile.getName();
            String id = profile.getId();
            Uri picUri = profile.getProfilePictureUri(350, 350);
            new DownloadToView(profilePicture).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, picUri.toString());
            new HttpRequestAchievements().execute(id);
            nameText.setText(name);

        } else {
            loginButton.setBackgroundResource(R.drawable.fb_login);
            nameText.setText("User");
            profilePicture.setImageDrawable(ContextCompat.getDrawable(ProfileActivity.this, R.drawable.default_profile));
            new HttpRequestAchievements().execute("");
        }
    }

    /**
     * this method updates achievementsListView list
     */
    private void updateAchievements(final List<AchievementItem> achievementItems) {
        ArrayAdapter<AchievementItem> adapter = new ArrayAdapter<AchievementItem>(getApplicationContext(), R.layout.list_view_achievements, achievementItems) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                if (convertView == null) {
                    convertView = getLayoutInflater().inflate(R.layout.list_view_achievements, parent, false);
                }
                TextView title = (TextView) convertView.findViewById(R.id.title);
                TextView description = (TextView) convertView.findViewById(R.id.description);

                AchievementItem achievementItem = achievementItems.get(position);
                title.setText(achievementItem.getName());
                description.setText(achievementItem.getDescription());
                if (achievementItem.getUserHas()) {
                    convertView.setBackgroundResource(R.drawable.achievement_complete);
                } else {
                    convertView.setBackgroundResource(R.drawable.achievement_uncomplete);
                }
                return convertView;
            }
        };

        achievementsListView.setAdapter(adapter);
    }

    /**
     * This Class extends DownloadToBitmap and places downloaded Bitmap to ImageView
     */
    private class DownloadToView extends DownloadToBitmap {
        private ImageView picView;

        public DownloadToView(ImageView picView) {
            this.picView = picView;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            picView.setImageBitmap(result);
        }
    }

    /**
     * Thic nested Class requests achievementsListView from server and updates Achievements List
     */
    private class HttpRequestAchievements extends AsyncTask<String, Void, List<AchievementItem>> {


        @Override
        protected List<AchievementItem> doInBackground(String... params) {
            JSONArray jsonArray = new JSONArray();
            try {
                URL url = new URL(getResources().getString(R.string.server_url) + "gotl/achievement/listUserAchievements?userid=" + params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                jsonArray = new JSONArray(in.readLine());
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            List<JSONObject> jsonAchievements = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    jsonAchievements.add(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            List<AchievementItem> achievementItems = new ArrayList<>();
            for (JSONObject userAchievement : jsonAchievements) {
                try {
                    achievementItems.add(new AchievementItem(userAchievement.getString("name")
                            , userAchievement.getString("description")
                            , userAchievement.getInt("id")
                            , userAchievement.getBoolean("userHas")));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return achievementItems;
        }

        @Override
        protected void onPostExecute(List<AchievementItem> achievementItems) {
            ProfileActivity.this.updateAchievements(achievementItems);
        }
    }

}