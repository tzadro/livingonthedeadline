package com.livingonthedeadline.gameofthroneslocations;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * The main Map activity which is displayed on the screen, and controlled by the user.
 */
public class MapActivity extends AppCompatActivity {
    private float maxZoom = 13;
    private MapFragment mapFragment;
    private DrawerLayout drawer = null;
    private GoogleMap map = null;
    private int REQUEST_ID = 1;
    private HttpRequestMarker requestMarker = new HttpRequestMarker();
    private HttpRequestBounds requestBounds = new HttpRequestBounds();

    /**
     * Called when the activity is first created.
     * Initializes the activity layout and the fragment manager.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_drawer);
        mapFragment = new MapFragment();
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.add(R.id.content_frame, mapFragment);
        ft.commit();
        drawer.setFocusableInTouchMode(false);
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                map = mapFragment.getMap();
                map.setMyLocationEnabled(true);

                requestBounds.cancel(true);
                requestMarker.cancel(true);
                Intent start = new Intent(MapActivity.this, MainActivity.class);
                startActivityForResult(start, REQUEST_ID);
            }
        });

        Button back = (Button) drawer.findViewById(R.id.back_button_small);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    requestBounds.cancel(true);
                    requestMarker.cancel(true);
                    Intent start = new Intent(MapActivity.this, MainActivity.class);
                    startActivityForResult(start, REQUEST_ID);
                }
            }
        });
    }

    /**
     * Starts the http requests for fetching the map bounds and markers, sets up
     * the city id from the gotten intent data, and creates a new MarkerInfoWindow
     * object which will handle the markers and location changes.
     *
     * @param cityId - city to be displayed
     */
    public void refreshMarkers(String cityId) {
        map.clear();
        map.setPadding(210, 20, 0, 0);
        map.setMyLocationEnabled(true);

        //map.addMarker(new MarkerOptions().title("eto").snippet("what a marvellous location").position(new LatLng(45.783181, 15.963682)));
        //map.addMarker(new MarkerOptions().title("eto").snippet("what a marvellous location").position(new LatLng(45.783515, 15.965417)));

        requestMarker = new HttpRequestMarker();
        requestMarker.execute(cityId);
        requestBounds = new HttpRequestBounds();
        requestBounds.execute(cityId);
        new MarkerInfoWindow(mapFragment, drawer, MapActivity.this, cityId);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ID)
            if (resultCode == RESULT_OK) {
                if (data.getStringExtra("cityId").equals("-1"))
                    finish();
                refreshMarkers(data.getStringExtra("cityId"));
            } else {
                requestBounds.cancel(true);
                requestMarker.cancel(true);
                Intent start = new Intent(MapActivity.this, MainActivity.class);
                startActivityForResult(start, REQUEST_ID);
            }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(Gravity.LEFT)) drawer.closeDrawer(Gravity.LEFT);

        else {
            requestBounds.cancel(true);
            requestMarker.cancel(true);
            Intent start = new Intent(MapActivity.this, MainActivity.class);
            startActivityForResult(start, REQUEST_ID);
        }
    }

    /**
     * Asynchronously fetches marker data and adds them to the map.
     */
    private class HttpRequestMarker extends AsyncTask<String, Void, List<MarkerOptions>> {

        @Override
        protected List<MarkerOptions> doInBackground(String... params) {
            JSONArray jsonArray = new JSONArray();
            try {
                URL url = new URL(getResources().getString(R.string.server_url) + "gotl/marker/listMarkers?cityid=" + params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                jsonArray = new JSONArray(in.readLine());
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            List<JSONObject> jsonMarkers = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    jsonMarkers.add(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            List<MarkerOptions> markers = new ArrayList<>();
            for (JSONObject marker : jsonMarkers) {
                float icon = 0;
                try {
                    switch (marker.getInt("typeid")) {
                        case 1:
                            icon = BitmapDescriptorFactory.HUE_RED;
                            break;
                        case 2:
                            icon = BitmapDescriptorFactory.HUE_AZURE;
                    }
                    markers.add(new MarkerOptions()
                                    .icon(BitmapDescriptorFactory.defaultMarker(icon))
                                    .position(new LatLng(marker.getDouble("lat"), marker.getDouble("lng")))
                                    .title(marker.getString("name"))
                                    .snippet(marker.getString("description"))
                    );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return markers;
        }

        @Override
        protected void onPostExecute(List<MarkerOptions> markers) {
            super.onPostExecute(markers);
            for (MarkerOptions marker : markers) {
                map.addMarker(marker);
            }
        }

    }

    /**
     * Asynchronously fetches the map boundaries data and applies it to the map.
     * Starts the camera change listener afterwards, so the user can't move the
     * camera outside the specified bounds.
     */
    private class HttpRequestBounds extends AsyncTask<String, Void, LatLngBounds> {

        @Override
        protected LatLngBounds doInBackground(String... params) {
            JSONObject jsonBounds = new JSONObject();
            try {
                URL url = new URL(getResources().getString(R.string.server_url) + "gotl/city/getCity?id=" + params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                jsonBounds = new JSONObject(in.readLine());
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            LatLngBounds bounds = null;
            try {
                bounds = new LatLngBounds(
                        new LatLng(jsonBounds.getDouble("swlat"), jsonBounds.getDouble("swlng")),
                        new LatLng(jsonBounds.getDouble("nelat"), jsonBounds.getDouble("nelng"))
                );
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return bounds;
        }

        @Override
        protected void onPostExecute(LatLngBounds bounds) {
            super.onPostExecute(bounds);
            CameraPosition pos = new CameraPosition.Builder().target(bounds.getCenter()).build();
            map.moveCamera(CameraUpdateFactory.newLatLngZoom((pos.target), maxZoom));

            map.setOnCameraChangeListener(new BoundsAndZoomCheck(getApplicationContext(), map,
                            pos, bounds, maxZoom)
            );
        }
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (drawer.isDrawerOpen(Gravity.LEFT)) {
                drawer.closeDrawer(Gravity.LEFT);
            } else {
                requestBounds.cancel(true);
                requestMarker.cancel(true);
                Intent start = new Intent(MapActivity.this, MainActivity.class);
                startActivityForResult(start, REQUEST_ID);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

}