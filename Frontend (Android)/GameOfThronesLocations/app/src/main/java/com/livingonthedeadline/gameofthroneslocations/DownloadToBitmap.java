package com.livingonthedeadline.gameofthroneslocations;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;

/**
 * This class is used to download the picture URL String to Bitmap Object
 */
public abstract class DownloadToBitmap extends AsyncTask<String, Void, Bitmap> {

    /**
     * The method which downloads the picture, and places it into a Bitmap Object
     * @param urls - the url which will be placed into a Bitmap Object
     * @return - the wanted Bitmap Object
     */
    @Override
    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap bitmap = null;
        try{
            InputStream in = new java.net.URL(urldisplay).openStream();
            bitmap = BitmapFactory.decodeStream(in);
        }catch (Exception e){
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return bitmap;
    }
    protected abstract void onPostExecute(Bitmap result);
}