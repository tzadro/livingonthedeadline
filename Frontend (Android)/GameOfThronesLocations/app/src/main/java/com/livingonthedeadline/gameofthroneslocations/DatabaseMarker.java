package com.livingonthedeadline.gameofthroneslocations;

import com.google.android.gms.maps.model.LatLng;

/**
 * The object used for storing data about yet unvisited locations from the database.
 */
public class DatabaseMarker {
    private int id;
    private LatLng latlng;
    /**
     * constructs a <code>DatabaseMarker</code> object with the information
     * fetched from the database
     * @param id - the marker id
     * @param latlng - a latlng object containing the geographical latitude and longitude of
     *               the marker
     */
    public DatabaseMarker(int id, LatLng latlng) {
        this.id = id;
        this.latlng = latlng;
    }

    /**
     * Fetches the marker id.
     * @return - the marker id.
     */
    public int getId() {
        return id;
    }

    /**
     * Fetches the latlng object of the marker.
     * @return - the latlng object.
     */
    public LatLng getLatlng() {
        return latlng;
    }
}
