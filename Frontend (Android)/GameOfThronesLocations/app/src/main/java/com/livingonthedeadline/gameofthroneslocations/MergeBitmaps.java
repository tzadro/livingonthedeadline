package com.livingonthedeadline.gameofthroneslocations;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * The object used to merge the user's photo with the original GoT scene.
 */
public class MergeBitmaps {
    private static Bitmap upperPicture;
    private static Bitmap bottomPicture;
    private static Bitmap result;

    /**
     * Constructs a MergrBitmaps object, scales the user's photo if necessary.
     * @param upperPicture
     * @param bottomPicture
     */
    public MergeBitmaps(Bitmap upperPicture, Bitmap bottomPicture) {
        this.upperPicture = upperPicture;
        if (upperPicture.getWidth() == bottomPicture.getWidth())
            this.bottomPicture = bottomPicture;
        else
            this.bottomPicture = scalePicture(upperPicture,bottomPicture);
    }

    /**
     * Scales the user's photo based on the original
     * @param pic1 - original scene
     * @param pic2 - the user's photo
     * @return
     */
    private Bitmap scalePicture(Bitmap pic1, Bitmap pic2){
        float scale = (float)pic1.getWidth()/pic2.getWidth();
        return Bitmap.createScaledBitmap(pic2, (int)(pic2.getWidth()*scale), (int)(pic2.getHeight()*scale),true);
    }

    /**
     * Merges the images together, and returns it as one bitmap image.
     * @return - the requested merged image
     */
    public static Bitmap getResult() {
        Bitmap cs = null;
        int width, height = 0;

        if(upperPicture.getWidth() > bottomPicture.getWidth()) {
            width = upperPicture.getWidth();
            height = upperPicture.getHeight() + bottomPicture.getHeight();
        } else {
            width = bottomPicture.getWidth();
            height = upperPicture.getHeight()+ bottomPicture.getHeight();
        }

        cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

        Canvas comboImage = new Canvas(cs);

        comboImage.drawBitmap(upperPicture, 0f, 0f, null);
        comboImage.drawBitmap(bottomPicture, 0f, upperPicture.getHeight(), null);
        result = cs;

        return result;
    }
}