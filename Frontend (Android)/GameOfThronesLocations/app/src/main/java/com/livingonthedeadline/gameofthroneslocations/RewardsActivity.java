package com.livingonthedeadline.gameofthroneslocations;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RewardsActivity extends AppCompatActivity {
    ListView rewardListView;
    List<RewardItem> rewardItemsList = new ArrayList<>();
    Profile profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rewards);
        rewardListView = (ListView) findViewById(R.id.list_reward);

        setTitle("Rewards:  ");
        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        if (Profile.getCurrentProfile() != null) {
            profile = Profile.getCurrentProfile();
            new HttpRequestRewards().execute(profile.getId());
        } else
            finish();

        //test();

        rewardListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RewardItem item = rewardItemsList.get(position);
                dialogUseRewardPrompt(item);
            }
        });
    }

    /*private void test() {
        List<RewardItem> rewardItems = new ArrayList<>();
        rewardItems.add(new RewardItem(1, "sladoled: sam 0kn"));
        rewardItems.add(new RewardItem(2, "sladoled: sam 1kn"));
        rewardItems.add(new RewardItem(3, "sladoled: sam 2kn"));
        rewardItems.add(new RewardItem(4, "sladoled: sam 3kn"));
        rewardItems.add(new RewardItem(5, "sladoled: sam 4kn"));
        rewardItems.add(new RewardItem(6, "sladoled: sam 5kn"));
        rewardItems.add(new RewardItem(7, "sladoled: sam 6kn"));

        RewardsActivity.this.updateRewards(rewardItems);
        rewardItemsList = rewardItems;
    }*/

    public void useTest(RewardItem item) {
        rewardItemsList.remove(item);
        RewardsActivity.this.updateRewards(rewardItemsList);
        dialogUse("AEYGF7K0DM1X");
    }

    private class RewardItem {
        private Integer id;
        private String description;

        public RewardItem(Integer id, String description) {
            this.id = id;
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public Integer getId() {
            return id;
        }
    }

    private void dialogUseRewardPrompt(final RewardItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Use this reward?\n" + item.description)
                .setCancelable(false)
                .setPositiveButton("USE", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //new HttpRequestRewardUse(item).execute(profile.getId());
                        useTest(item);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void dialogUse(final String rewardCode) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(rewardCode)
                .setCancelable(false)
                .setPositiveButton("dismis", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .setNeutralButton("save to clipboard", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("Reward code", rewardCode);
                        clipboard.setPrimaryClip(clip);
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void dialogEmpty() {
        AlertDialog alertDialog = new AlertDialog.Builder(RewardsActivity.this).create();
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setTitle("You have no available rewards!");
        alertDialog.setMessage("Check back later");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        RewardsActivity.this.finish();
                    }
                });
        alertDialog.show();
    }


    private void updateRewards(final List<RewardItem> rewardItems) {
        if (rewardItems.isEmpty())
            dialogEmpty();
        ArrayAdapter<RewardItem> adapter =
                new ArrayAdapter<RewardItem>(getApplicationContext(), R.layout.list_view_rewards, rewardItems) {
                    @Override
                    public View getView(int position, View convertView, ViewGroup parent) {
                        if (convertView == null) {
                            convertView = getLayoutInflater().inflate(R.layout.list_view_rewards, parent, false);
                        }
                        TextView description = (TextView) convertView.findViewById(R.id.reward_content);

                        RewardItem rewardItem = rewardItems.get(position);
                        description.setText(rewardItem.getDescription());
                        return convertView;
                    }
                };

        rewardListView.setAdapter(adapter);
    }

    private class HttpRequestRewards extends AsyncTask<String, Void, List<RewardItem>> {
        @Override
        protected List<RewardItem> doInBackground(String... params) {
            JSONArray jsonArray = new JSONArray();
            try {
                URL url = new URL(getResources().getString(R.string.server_url) +
                        "gotl/reward/listUserRewards?userid=" + params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                jsonArray = new JSONArray(in.readLine());
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }

            List<JSONObject> jsonAchievements = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    jsonAchievements.add(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            List<RewardItem> rewardItems = new ArrayList<>();
            for (JSONObject userReward : jsonAchievements) {
                try {
                    rewardItems.add(new RewardItem(userReward.getInt("id")
                            , userReward.getString("description")));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return rewardItems;
        }

        @Override
        protected void onPostExecute(final List<RewardItem> rewardItems) {
            RewardsActivity.this.updateRewards(rewardItems);
            rewardItemsList = rewardItems;
        }
    }

    /*private class HttpRequestRewardUse extends AsyncTask<String, Void, String> {
        RewardItem item;

        public HttpRequestRewardUse(RewardItem item) {
            this.item = item;
        }

        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonString = new JSONObject();
            try {
                URL url = new URL(getResources().getString(R.string.server_url) +
                        "gotl/reward/listUserAchievements?userid=" + params[0] +
                        "&rewardId=" + item.getId());
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                jsonString = new JSONObject(in.readLine());
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            String rewardText = "";
            try {
                rewardText = jsonString.getString("rewardCode");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return rewardText;
        }

        @Override
        protected void onPostExecute(String rewardText) {
            rewardItemsList.remove(item);
            RewardsActivity.this.updateRewards(rewardItemsList);
            dialogUse(rewardText);
        }
    }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
