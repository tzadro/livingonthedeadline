package com.livingonthedeadline.gameofthroneslocations;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

/**
 * An activity that prompts the user to log in if he tries to initiate an action which requires
 * a profile.
 */
public class LoginActivity extends AppCompatActivity {
    private static CallbackManager callbackManager;


    private FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            done();
        }

        @Override
        public void onCancel() {
            //ignore
        }

        /**
         * A method which displays an alert dialog on the screen if something goes wrong;
         * if an exception is caught.
         * @param error - the caught exception
         */
        @Override
        public void onError(FacebookException error) {
            AlertDialog alertDialog = new AlertDialog.Builder(LoginActivity.this).create();
            alertDialog.setTitle("Something went wrong!");
            alertDialog.setMessage("Please try again");
            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
        }
    };


    private void done(){
        SharedPreferences settings = getSharedPreferences("my_settings", Context.MODE_PRIVATE);
        settings.edit().putBoolean("first_time", false).commit();

        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        callbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button_f);

        loginButton.setPublishPermissions("publish_actions");
        // TODO dodati ostale potrebne dozvole ak ce trebat


        loginButton.registerCallback(callbackManager, callback);

        Button decline = (Button) findViewById(R.id.decline_button);
        decline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                done();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {//ignore
    }
}