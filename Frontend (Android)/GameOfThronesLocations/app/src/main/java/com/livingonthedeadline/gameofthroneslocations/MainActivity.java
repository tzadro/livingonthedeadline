package com.livingonthedeadline.gameofthroneslocations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.facebook.FacebookSdk;

/**
 * The main activity from which the user navigates to other activities
 */
public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        checkFirstTime();
        setContentView(R.layout.activity_main);

        Button locDemo = (Button) findViewById(R.id.button_demo);
        Button locDb = (Button) findViewById(R.id.button_db);
        Button locSt = (Button) findViewById(R.id.button_st);
        Button profile = (Button) findViewById(R.id.button_profile);

        /**
         * Launches the demo map activity.
         */
        locDemo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent();
                String cityId = "3";
                data.putExtra("cityId", cityId);
                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, data);
                } else {
                    getParent().setResult(Activity.RESULT_OK, data);
                }
                finish();
            }
        });
        /**
         * Launches the map of Dubrovnik.
         */
        locDb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent(MainActivity.this, MapActivity.class);
                String cityId = "1";
                data.putExtra("cityId", cityId);
                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, data);
                } else {
                    getParent().setResult(Activity.RESULT_OK, data);
                }
                finish();
            }
        });
        /**
         * Launches the map of Split.
         */
        locSt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent data = new Intent(MainActivity.this, MapActivity.class);
                String cityId = "2";
                data.putExtra("cityId", cityId);
                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, data);
                } else {
                    getParent().setResult(Activity.RESULT_OK, data);
                }
                finish();
            }
        });
        /**
         * Launches the profile activity.
         */
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent toProfile = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(toProfile);
            }
        });
    }

    /**
     * A method which checks if the app has already been started.
     * If it hasn't, launch the welcome activity.
     */
    private void checkFirstTime() {
        SharedPreferences settings = getSharedPreferences("my_settings", Context.MODE_PRIVATE);
        if (settings.getBoolean("first_time", true)) {
            Intent firstTime = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(firstTime);
        }
    }

    @Override
    public void onBackPressed() {
        Intent data = new Intent();
        String cityId = "-1";
        data.putExtra("cityId", cityId);
        if (getParent() == null) {
            setResult(Activity.RESULT_OK, data);
        } else {
            getParent().setResult(Activity.RESULT_OK, data);
        }
        finish();
    }
}