package com.livingonthedeadline.gameofthroneslocations;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.facebook.Profile;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * A support class for MainActivity which manages the markers and location changes.
 */
public class MarkerInfoWindow implements GoogleMap.OnMyLocationChangeListener, GoogleMap.OnMarkerClickListener {
    GoogleMap map;
    DrawerLayout drawer;
    ImageView picture;
    TextView titleText, descText, distText;
    ImageButton cameraButton, backButton;
    String url, cityId;
    Context mContext;
    List<DatabaseMarker> databaseMarkers;
    LayoutInflater inflater;
    View buttonView;
    ViewGroup parent;

    int index;

    /**
     * Constructs a MarkerInfoWindow object with the neccessary information from the activity.
     * Sets up the listeners and layout items which will later be dynamically changed.
     *
     * @param mapFragment - the fragment of the map currently running
     * @param drawer      - the MapActivity main layout
     * @param mContext    - the MapActivity context
     * @param cityId      - id of the current displayed city
     */
    public MarkerInfoWindow(MapFragment mapFragment, DrawerLayout drawer, Context mContext, String cityId) {
        this.cityId = cityId;
        this.drawer = drawer;
        if (!(Profile.getCurrentProfile() == null))
            new HttpRequestDatabaseMarker().execute(cityId);

        this.mContext = mContext;
        map = mapFragment.getMap();
        map.setMyLocationEnabled(true);
        map.setOnMarkerClickListener(this);

        picture = (ImageView) drawer.findViewById(R.id.imageView);
        titleText = (TextView) drawer.findViewById(R.id.textView);
        descText = (TextView) drawer.findViewById(R.id.textView2);
        inflater = LayoutInflater.from(mContext);
        buttonView = ((Activity) mContext).getWindow().getDecorView().findViewById(R.id.nav_button);
        distText = (TextView) drawer.findViewById(R.id.backText);

        drawer.setFocusableInTouchMode(false);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        Switch switch1 = (Switch) drawer.findViewById(R.id.switch1);
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                } else {
                    map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
            }
        });
    }

    /**
     * Places the matching data (image, title, snippet) into the drawer layout.
     * Moves the marker to the position so it can still be seen when the drawer opens.
     * Decides which button to place into the layout based on marker proximity,
     * and opens the drawer afterwards.
     *
     * @param marker - the marker object which is clicked.
     * @return
     */
    @Override
    public boolean onMarkerClick(Marker marker) {

        url = mContext.getResources().getString(R.string.server_url) +
                marker.getTitle().replace(" ", "").replace("?", "").replace("'", "") + "S.jpg";
        new DownloadToView(picture).execute(url);
        titleText.setText(marker.getTitle());
        descText.setText(marker.getSnippet());

        parent = (ViewGroup) buttonView.getParent();
        index = parent.indexOfChild(buttonView);
        parent.removeView(buttonView);

        if (map.getMyLocation() != null)
            if (checkIfCloseEnough(map.getMyLocation(), marker.getPosition()) < 20) {
                buttonView = inflater.inflate(R.layout.camera_view, parent, false);
                parent.addView(buttonView, index);
                cameraClickListener(cameraButton);
            } else {
                buttonView = inflater.inflate(R.layout.back_view, parent, false);
                parent.addView(buttonView, index);
                backClickListener(marker);
            }
        drawer.openDrawer(Gravity.LEFT);
        return true;
    }

    /**
     * This Class extends DownloadToBitmap and places downloaded Bitmap to ImageView
     */
    private class DownloadToView extends DownloadToBitmap {
        private ImageView picView;

        public DownloadToView(ImageView picView) {
            this.picView = picView;
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            picView.setImageBitmap(result);
        }
    }

    /**
     * Tracks the user location as it changes.
     * If the user comes close enough to a marker, updates the achievement list and
     * removes the marker from the unvisited markers list.
     *
     * @param location - the new user location
     */
    @Override
    public void onMyLocationChange(Location location) {
        if ((checkIfCloseEnough(location, locateNearestMarker(location).getLatlng())) < 20) {
            DatabaseMarker marker = locateNearestMarker(location);
            int id = marker.getId();
            new HttpUpdateAchievements().execute(id);
            databaseMarkers.remove(marker);
        }
        return;
    }

    /**
     * Returns the marker closest to the user's current location.
     *
     * @param location - the user's current location
     * @return databasemarker - the requested marker
     */
    private DatabaseMarker locateNearestMarker(Location location) {
        float minDistance = 0;
        int position = 0;
        for (int i = 0; i < databaseMarkers.size(); i++) {
            float[] distance = new float[1];
            Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                    databaseMarkers.get(i).getLatlng().latitude, databaseMarkers.get(i).getLatlng().longitude,
                    distance);
            if (i == 0) minDistance = distance[0];
            else if (minDistance > distance[0]) {
                minDistance = distance[0];
                position = i;
            }
        }
        return databaseMarkers.get(position);
    }

    /**
     * Checks if the user is close enough to a marker location.
     *
     * @param myLocation - the user's current location
     * @param latLng     - the marker location
     * @return boolean value, returns the requested info
     */
    private double checkIfCloseEnough(Location myLocation, LatLng latLng) {
        Location markerLocation = new Location(myLocation);

        markerLocation.setLatitude(latLng.latitude);
        markerLocation.setLongitude(latLng.longitude);
        return (myLocation.distanceTo(markerLocation));
    }

    /**
     * Sets up the camera button into the drawer layout.
     * -> adds a listener to the button which launches the PhotoActivity on click.
     *
     * @param cameraButton - the button which will be converted to a camera button
     * @return - true, since the method is consumed
     */
    private boolean cameraClickListener(ImageButton cameraButton) {
        cameraButton = (ImageButton) drawer.findViewById(R.id.nav_button);
        cameraButton.setImageResource(R.drawable.camera);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent launchPhoto = new Intent(mContext, PhotoActivity.class);
                launchPhoto.putExtra("pictureUrl", url.substring(0, url.length() - 5) + ".jpg");
                mContext.startActivity(launchPhoto);
                return;
            }
        });
        return true;
    }

    /**
     * Sets up the back button into the drawer layout.
     * -> adds a listener to the button which closes the drawer on click.
     *
     * @param marker - the marker clicked
     * @return - true, since the method is consumed
     */
    private boolean backClickListener(Marker marker) {
        distText = (TextView) drawer.findViewById(R.id.backText);
        backButton = (ImageButton) drawer.findViewById(R.id.nav_button);

        if (map.getMyLocation() != null)
            distText.setText("Get " + (String.valueOf(Math.round(checkIfCloseEnough
                    (map.getMyLocation(), marker.getPosition()))) + " meters closer."));
        else
            distText.setText(mContext.getResources().getString(R.string.location_disabled));
        backButton.setImageResource(R.drawable.back);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.closeDrawer(Gravity.LEFT);
                return;
            }
        });
        return true;
    }

    /**
     * Asynchronously sends the marker data to the database, which accordingly updates
     * the achievements.
     */
    private class HttpUpdateAchievements extends AsyncTask<Integer, Void, Void> {
        @Override
        protected Void doInBackground(Integer... params) {

            try {
                URL url = new URL(mContext.getResources().getString(R.string.server_url) +
                        "gotl/visited/create?userid=" + Profile.getCurrentProfile().getId() +
                        "&markerid=" + params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        urlConnection.getInputStream()));
                in.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (databaseMarkers.size() == 0) {
                map.setOnMyLocationChangeListener(null);
            }
            return null;

        }
    }

    /**
     * Asynchronously fetches unvisited marker data and saves it into a list for further use.
     */
    private class HttpRequestDatabaseMarker extends AsyncTask<String, Void, List<DatabaseMarker>> {
        @Override
        protected List<DatabaseMarker> doInBackground(String... params) {
            JSONArray jsonArray = new JSONArray();
            try {
                URL url = new URL(mContext.getResources().getString(R.string.server_url) +
                        "gotl/marker/listNotVisitedByUser?cityId=" + params[0] + "&userid=" +
                        (((Profile.getCurrentProfile()) == (null))
                                ? "" : Profile.getCurrentProfile().getId()));
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                jsonArray = new JSONArray(in.readLine());
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }

            List<JSONObject> jsonMarkers = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    jsonMarkers.add(jsonArray.getJSONObject(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            databaseMarkers = new ArrayList<>();
            for (JSONObject marker : jsonMarkers) {
                try {
                    databaseMarkers.add(new DatabaseMarker(marker.getInt("id"),
                            new LatLng(marker.getDouble("lat"), marker.getDouble("lng"))));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return databaseMarkers;
        }

        /**
         * Method executed after the data is fetched from the database.
         * Starts the OnMyLocationChangeListener if the current user has one or more
         * unvisited locations.
         *
         * @param databaseMarkers
         */
        @Override
        protected void onPostExecute(List<DatabaseMarker> databaseMarkers) {
            if (!databaseMarkers.isEmpty())
                map.setOnMyLocationChangeListener(MarkerInfoWindow.this);
            super.onPostExecute(databaseMarkers);
        }
    }
}