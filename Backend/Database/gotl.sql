CREATE TABLE `achievement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(140) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `swlat` double DEFAULT NULL,
  `swlng` double DEFAULT NULL,
  `nelat` double DEFAULT NULL,
  `nelng` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `marker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(560) DEFAULT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `cityid` int(11) NOT NULL,
  `typeid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `cityid_idx` (`cityid`),
  KEY `typeid_idx` (`typeid`),
  CONSTRAINT `cityid` FOREIGN KEY (`cityid`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;


CREATE TABLE `requires` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `achievementid` int(11) NOT NULL,
  `markerid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `achievementid_idx` (`achievementid`),
  KEY `markerid_idx` (`markerid`),
  CONSTRAINT `achievementid` FOREIGN KEY (`achievementid`) REFERENCES `achievement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `markerid2` FOREIGN KEY (`markerid`) REFERENCES `marker` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

CREATE TABLE `reward` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(140) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `user` (
  `id` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `visited` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(45) NOT NULL,
  `markerid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `userid_idx` (`userid`),
  KEY `markerid_idx` (`markerid`),
  CONSTRAINT `markerid` FOREIGN KEY (`markerid`) REFERENCES `marker` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `userid` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

CREATE TABLE `gotl`.`wins` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `achievementid` INT NOT NULL,
  `rewardid` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `achievementid_idx` (`achievementid` ASC),
  INDEX `rewardid_idx` (`rewardid` ASC),
  CONSTRAINT `achievementid4`
    FOREIGN KEY (`achievementid`)
    REFERENCES `gotl`.`achievement` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `rewardid`
    FOREIGN KEY (`rewardid`)
    REFERENCES `gotl`.`reward` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);


INSERT INTO type (id, name)
VALUES (1, 'Scene');


INSERT INTO city (id, name, swlat, swlng, nelat, nelng)
VALUES (1, 'Dubrovnik', 42.570965, 17.864895, 42.725242, 18.292162);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (1, 'Geoffrey''s Birthday', 'Geoffrey enjoys his birthday in S02E01', 42.640475, 18.104239, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (2, 'Cersei''s and Littlefinger''s discussion', 'Cersei and Littlefinger talk about power in S02E01', 42.640609, 18.104296, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (3, 'Tyrion''s and Varys''s discussion', 'Tyrion and Varys bond in S02E08', 42.640838, 18.106065, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (4, 'Myrcella''s departure', 'Myrcella being shipped off to Dorne in S02E06', 42.641165, 18.106300, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (5, 'Street riot', 'Riot in the streets of King'' Landing while escorting Geoffrey to the castle in S02E06', 42.641879, 18.107164, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (6, 'The slap', 'Tyrion slapping the King during the riot in S02E06', 42.640622, 18.104321, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (7, 'Way back home', 'Sansa talks with Littlefinger about taking her home in S03E01', 42.641513, 18.105078, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (8, 'Purple wedding', 'Wedding of Margaery and Geoffrey in S04E02', 42.641212, 18.102387, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (9, 'The Mountain and the Viper', 'Trial by combat deciding Tyrions faith. Oberyn Martell, called The Viper, is fighting Gregor Clegane, called The Mountain, not only to free Tyrion of his death sentence, but also to avenge his sister, Elia Martell, who was raped and killed by The Mountain.

The scene was filmed in an old abandoned hotel, Hotel Belvedere. Hotel was opened in 1985. and was completely demolished in 1991. during Croatian War of Independence. It is the only Dubrovnik''s hotel that wasn''t renovated after the war.', 42.633682, 18.130850, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (10, 'Shaming', 'Cersei being shamed by the High Sparrow in S05E10', 42.639675, 18.109450, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (11, 'Qarth party', 'Daenerys and Jorah on a party in Qarth hosted by Xaro in S02E05', 42.624556, 18.121059, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (12, 'House of the Undying', 'Daenerys looking for her stolen dragons in S02E10', 42.642741, 18.108477, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (13, 'Spice King''s palace', 'Daenerys requesting in S02E06', 42.640330, 18.110850, 1, 1);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (14, 'Oberyn and Tyrion', 'Tyrion wishes Oberyn good welcome after his arrival to King''s Landing in S05E01', 42.640393, 18.107490, 1, 1);


INSERT INTO city (id, name, swlat, swlng, nelat, nelng)
VALUES (2, 'Split', 43.333118, 16.085377, 43.698804, 16.786613);

INSERT INTO marker (id, name, description, lat, lng, cityid, typeid)
VALUES (15, 'Arya''s oysters', 'Arya selling oysters in Braavos in S05E08', 43.547578, 16.395172, 2, 1);


INSERT INTO achievement
VALUES (1, 'You know nothing!', 'You still haven''t visited any of the locations with this app');

INSERT INTO requires (id, achievementid)
VALUES (1);

INSERT INTO achievement
VALUES (2, 'What is power?', 'You just got demonstrated that power is power, not knowledge, by Cersei');

INSERT INTO requires(achievementid, markerid)
VALUES (2, 2);

INSERT INTO achievement
VALUES (3, 'Purple wedding', 'You attended the wedding, enjoy the show!');

INSERT INTO requires(achievementid, markerid)
VALUES (3, 8);