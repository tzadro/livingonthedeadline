package com.livingonthedeadline.gotl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GameOfThronesLocationsApplication {

	public static void main(String[] args) {
		
		SpringApplication.run(GameOfThronesLocationsApplication.class, args);
	}
}
