package com.livingonthedeadline.gotl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.livingonthedeadline.gotl.model.Achievement;

public class AchievementMapper implements RowMapper<Achievement> {
	public Achievement mapRow(ResultSet rs, int rowNum) throws SQLException {
		Achievement achievement = new Achievement();
		achievement.setId(rs.getInt("id"));
		achievement.setName(rs.getString("name"));
		achievement.setDescription(rs.getString("description"));
		return achievement;
	}
}