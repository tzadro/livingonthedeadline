package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import com.livingonthedeadline.gotl.mapper.RewardMapper;
import com.livingonthedeadline.gotl.model.Reward;

public class RewardJDBCTemplate implements RewardDAO {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
	}

	@Override
	public List<Reward> listUserRewards(String userid) {
		String SQL = "SELECT r.id, r.description "
				+ "FROM reward as r JOIN wins ON r.id = achievementid WHERE achievementid "
				+ "IN (SELECT id FROM achievement as outter WHERE (SELECT COUNT(*) "
				+ "FROM requires WHERE achievementid = outter.id) = (SELECT COUNT(*) "
				+ "FROM visited WHERE userid = ? AND (markerid IN (SELECT markerid "
				+ "FROM requires WHERE achievementid = outter.id) OR markerid IS NULL)) "
				+ "IS TRUE)";
		
		return jdbcTemplateObject.query(SQL, new RewardMapper(), userid);
	}

}
