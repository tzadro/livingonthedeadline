package com.livingonthedeadline.gotl.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.livingonthedeadline.gotl.mapper.UserMapper;
import com.livingonthedeadline.gotl.model.User;

public class UserJDBCTemplate implements UserDAO {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
	}

	@Override
	public void create(String id) {
		String SQL = "INSERT INTO user (id) values (?)";
		String SQL2 = "INSERT INTO visited (userid, markerid) "
				+ "VALUES (?, null)";

		jdbcTemplateObject.update(SQL, id);
		jdbcTemplateObject.update(SQL2, id);
		return;
	}

	@Override
	public boolean getUser(String id) {
		String SQL = "SELECT COUNT(*) FROM user WHERE id = ?";

		Integer userCount = jdbcTemplateObject.queryForObject(SQL, Integer.class, id);
		return userCount != 0;
	}

	@Override
	public List<User> listUsers() {
		List<User> users;
		String SQL = "SELECT * FROM user";
		
		users = jdbcTemplateObject.query(SQL, new UserMapper());
		return users;
	}

	@Override
	public void delete(String id) {
		String SQL = "DELETE FROM user WHERE id = ?";

		jdbcTemplateObject.update(SQL, id);
		return;
	}

}
