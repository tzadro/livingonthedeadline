package com.livingonthedeadline.gotl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.livingonthedeadline.gotl.model.Reward;

public class RewardMapper implements RowMapper<Reward> {
	public Reward mapRow(ResultSet rs, int rowNum) throws SQLException {
		Reward reward = new Reward();
		reward.setId(rs.getInt("id"));
		reward.setDescription(rs.getString("description"));
		return reward;
	}
}