package com.livingonthedeadline.gotl.controller;

import java.util.List;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.livingonthedeadline.gotl.dao.VisitedDAO;
import com.livingonthedeadline.gotl.model.Visited;

@RestController
public class VisitedController {
	private VisitedDAO visitedDAO = (VisitedDAO) new ClassPathXmlApplicationContext("beans.xml").getBean("visitedDAO");

	@RequestMapping("visited/create")
	public void create(@RequestParam(value = "userid") String userid,
			@RequestParam(value = "markerid") Integer markerid) {
		visitedDAO.create(userid, markerid);
	}
	
	@RequestMapping("visited/getVisited")
	public Visited getVisited(@RequestParam(value = "id") Integer id) {
		return visitedDAO.getVisited(id);
	}
	
	@RequestMapping("visited/listVisited")
	public List<Visited> listVisited(@RequestParam(value = "userid", required = false) String userid) {
		return visitedDAO.listVisited(userid);
	}
	
	@RequestMapping("visited/delete")
	public void delete(@RequestParam(value = "id") Integer id) {
		visitedDAO.delete(id);
	}
	
	@RequestMapping("visited/update")
	public void update(@RequestParam(value = "id") Integer id, @RequestParam(value = "userid") String userid,
			@RequestParam(value = "markerid") Integer markerid) {
		visitedDAO.update(id, userid, markerid);
	}
}
