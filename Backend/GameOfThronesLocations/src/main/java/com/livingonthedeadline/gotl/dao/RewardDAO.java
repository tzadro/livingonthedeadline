package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import com.livingonthedeadline.gotl.model.Reward;

public interface RewardDAO {
	/**
	 * This is the method to be used to initialize database resources ie.
	 * connection.
	 */
	public void setDataSource(DataSource dataSource);

	/**
	 * This is the method to list all rewards user has earned.
	 */
	public List<Reward> listUserRewards(String userid);
}