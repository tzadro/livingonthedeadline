package com.livingonthedeadline.gotl.controller;

import java.util.List;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.livingonthedeadline.gotl.dao.UserDAO;
import com.livingonthedeadline.gotl.model.User;

@RestController
public class UserController {
	private UserDAO userDAO = (UserDAO) new ClassPathXmlApplicationContext("beans.xml").getBean("userDAO");
	
	@RequestMapping("user/create")
	public void create(@RequestParam(value = "id") String id) {
		userDAO.create(id);
	}
	
	@RequestMapping("user/getUser")
	public boolean getUser(@RequestParam(value = "id") String id) {
		return userDAO.getUser(id);
	}
	
	@RequestMapping("user/listUsers")
	public List<User> listUsers() {
		return userDAO.listUsers();
	}
	
	@RequestMapping("user/delete")
	public void delete(@RequestParam(value = "id") String id) {
		userDAO.delete(id);
	}
}
