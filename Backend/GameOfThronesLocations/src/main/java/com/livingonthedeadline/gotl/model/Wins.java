package com.livingonthedeadline.gotl.model;

public class Wins {
	private Integer id;
	private Integer achievementid;
	private Integer rewardid;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getAchievementid() {
		return achievementid;
	}
	
	public void setAchievementid(Integer achievementid) {
		this.achievementid = achievementid;
	}
	
	public Integer getRewardid() {
		return rewardid;
	}
	
	public void setRewardid(Integer rewardid) {
		this.rewardid = rewardid;
	}
}
