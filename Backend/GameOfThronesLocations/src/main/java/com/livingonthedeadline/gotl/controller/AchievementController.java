package com.livingonthedeadline.gotl.controller;

import java.util.List;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.livingonthedeadline.gotl.dao.AchievementDAO;
import com.livingonthedeadline.gotl.dao.UserDAO;
import com.livingonthedeadline.gotl.model.Achievement;
import com.livingonthedeadline.gotl.model.UserAchievement;

@RestController
public class AchievementController {
	private AchievementDAO achievementDAO = (AchievementDAO) new ClassPathXmlApplicationContext("beans.xml").getBean("achievementDAO");
	private UserDAO userDAO = (UserDAO) new ClassPathXmlApplicationContext("beans.xml").getBean("userDAO");

	@RequestMapping("achievement/create")
	public void create(@RequestParam(value = "name") String name,
			@RequestParam(value = "description", required = false) String description) {
		achievementDAO.create(name, description);
	}

	@RequestMapping("achievement/getAchievement")
	public Achievement getAchievement(@RequestParam(value = "id") Integer id) {
		return achievementDAO.getAchievement(id);
	}

	@RequestMapping("achievement/listUserAchievements")
	public List<UserAchievement> listUserAchievement(@RequestParam(value = "userid") String userid) {
		if (userid.equals(""))
			return achievementDAO.listUserAchievements(null);
		else if (userDAO.getUser(userid))
			return achievementDAO.listUserAchievements(userid);
		else  {
			userDAO.create(userid);
			return achievementDAO.listUserAchievements(null);
		}
	}

	@RequestMapping("achievement/delete")
	public void delete(@RequestParam(value = "id") Integer id) {
		achievementDAO.delete(id);
	}

	@RequestMapping("achievement/update")
	public void update(@RequestParam(value = "id") Integer id, @RequestParam(value = "name") String name,
			@RequestParam(value = "description") String description) {
		achievementDAO.update(id, name, description);
	}
}
