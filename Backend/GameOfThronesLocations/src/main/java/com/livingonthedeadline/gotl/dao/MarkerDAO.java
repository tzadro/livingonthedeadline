package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import com.livingonthedeadline.gotl.model.Marker;

public interface MarkerDAO {
	/**
	 * This is the method to be used to initialize database resources ie.
	 * connection.
	 */
	public void setDataSource(DataSource dataSource);

	/**
	 * This is the method to be used to create a record in the Marker table.
	 */
	public void create(String name, String description, Double lat, Double lng, Integer cityid, Integer typeid);

	/**
	 * This is the method to be used to list down a record from the Marker
	 * table corresponding to a passed marker id.
	 */
	public Marker getMarker(Integer id);

	/**
	 * This is the method to be used to list down all the records from the
	 * Marker table from specified city, if null then whole table.
	 */
	public List<Marker> listMarkers(Integer cityid);
	
	/**
	 * This is the method to be used to list down all the records from the
	 * Marker table from specified city, if null then whole table.
	 */
	public List<Marker> listNotVisitedByUser(Integer cityid, String userid);

	/**
	 * This is the method to be used to delete a record from the Marker table
	 * corresponding to a passed marker id.
	 */
	public void delete(Integer id);

	/**
	 * This is the method to be used to update a record into the Marker table.
	 */
	public void update(Integer id, String name, String description, Double lat, Double lng, Integer cityid, Integer typeid);
}