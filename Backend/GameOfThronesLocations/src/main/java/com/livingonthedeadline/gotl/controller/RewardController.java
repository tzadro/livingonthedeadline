package com.livingonthedeadline.gotl.controller;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.livingonthedeadline.gotl.dao.RewardDAO;
import com.livingonthedeadline.gotl.model.Reward;

@RestController
public class RewardController {
	private RewardDAO rewardDAO = (RewardDAO) new ClassPathXmlApplicationContext("beans.xml").getBean("rewardDAO");

	@RequestMapping("reward/listUserRewards")
	public List<Reward> listUserRewards(@RequestParam(value = "userid") String userid) {
		return rewardDAO.listUserRewards(userid);
	}
}
