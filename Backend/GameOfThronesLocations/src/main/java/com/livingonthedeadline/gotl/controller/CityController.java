package com.livingonthedeadline.gotl.controller;

import java.util.List;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.livingonthedeadline.gotl.dao.CityDAO;
import com.livingonthedeadline.gotl.model.City;

@RestController
public class CityController {
	private CityDAO cityDAO = (CityDAO) new ClassPathXmlApplicationContext("beans.xml").getBean("cityDAO");

	@RequestMapping("city/create")
	public void create(@RequestParam(value = "name") String name, @RequestParam(value = "swlat") Double swlat,
			@RequestParam(value = "swlng") Double swlng, @RequestParam(value = "nelat") Double nelat,
			@RequestParam(value = "nelng") Double nelng) {
		cityDAO.create(name, swlat, swlng, nelat, nelng);
	}

	@RequestMapping("city/getCity")
	public City getCity(@RequestParam(value = "id") Integer id) {
		return cityDAO.getCity(id);
	}

	@RequestMapping("city/listCities")
	public List<City> listCities() {
		return cityDAO.listCities();
	}

	@RequestMapping("city/delete")
	public void delete(@RequestParam(value = "id") Integer id) {
		cityDAO.delete(id);
	}

	@RequestMapping("city/update")
	public void update(@RequestParam(value = "id") Integer id, @RequestParam(value = "name") String name,
			@RequestParam(value = "swlat") Double swlat, @RequestParam(value = "swlng") Double swlng,
			@RequestParam(value = "nelat") Double nelat, @RequestParam(value = "nelng") Double nelng) {
		cityDAO.update(id, name, swlat, swlng, nelat, nelng);
	}
}
