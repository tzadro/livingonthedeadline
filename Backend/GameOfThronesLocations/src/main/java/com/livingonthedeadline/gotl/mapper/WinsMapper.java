package com.livingonthedeadline.gotl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.livingonthedeadline.gotl.model.Wins;

public class WinsMapper implements RowMapper<Wins> {
	public Wins mapRow(ResultSet rs, int rowNum) throws SQLException {
		Wins wins = new Wins();
		wins.setId(rs.getInt("id"));
		wins.setAchievementid(rs.getInt("achievementid"));
		wins.setRewardid(rs.getInt("rewardid"));
		return wins;
	}
}