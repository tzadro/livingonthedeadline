package com.livingonthedeadline.gotl.dao;

import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import com.livingonthedeadline.gotl.mapper.AchievementMapper;
import com.livingonthedeadline.gotl.mapper.UserAchievementMapper;
import com.livingonthedeadline.gotl.model.Achievement;
import com.livingonthedeadline.gotl.model.UserAchievement;

public class AchievementJDBCTemplate implements AchievementDAO {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
	}

	@Override
	public void create(String name, String description) {
		String SQL = "INSERT INTO achievement (name, description) values (?, ?)";

		jdbcTemplateObject.update(SQL, name, description);
		return;
	}

	@Override
	public Achievement getAchievement(Integer id) {
		String SQL = "SELECT * FROM achievement WHERE id = ?";

		Achievement achievement = jdbcTemplateObject.queryForObject(SQL, new Object[] { id }, new AchievementMapper());
		return achievement;
	}

	@Override
	public List<UserAchievement> listUserAchievements(String userid) {
		List<UserAchievement> userAchievements = new ArrayList<>();
		if (userid == null)  {
			String SQL = "SELECT * FROM achievement";

			List<Achievement> achievements;
			achievements = jdbcTemplateObject.query(SQL, new AchievementMapper());
			for (Achievement a : achievements)
				userAchievements.add(new UserAchievement(a, false));
		} else {
			String SQL = "SELECT id, name, description, ((SELECT COUNT(*) FROM requires WHERE "
					+ "achievementid = outter.id) = (SELECT COUNT(*) FROM visited WHERE userid "
					+ "= ? AND (markerid IN (SELECT markerid FROM requires WHERE achievementid "
					+ "= outter.id) OR markerid IS NULL))) as userhas FROM achievement as outter";

			userAchievements = jdbcTemplateObject.query(SQL, new UserAchievementMapper(), userid);
		}
		return userAchievements;
	}

	@Override
	public void delete(Integer id) {
		String SQL = "DELETE FROM achievement WHERE id = ?";

		jdbcTemplateObject.update(SQL, id);
		return;
	}

	@Override
	public void update(Integer id, String name, String description) {
		String SQL = "UPDATE achievement SET name = ?, description = ? WHERE id = ?";

		jdbcTemplateObject.update(SQL, name, description, id);
		return;
	}

}
