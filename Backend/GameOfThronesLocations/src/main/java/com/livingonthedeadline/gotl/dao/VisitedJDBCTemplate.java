package com.livingonthedeadline.gotl.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import com.livingonthedeadline.gotl.mapper.VisitedMapper;
import com.livingonthedeadline.gotl.model.Visited;

public class VisitedJDBCTemplate implements VisitedDAO {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
	}

	@Override
	public void create(String userid, Integer markerid) {
		String SQL = "INSERT INTO visited (userid, markerid)"
				+ "VALUES (?, ?)";

		jdbcTemplateObject.update(SQL, userid, markerid);
		return;
	}

	@Override
	public Visited getVisited(Integer id) {
		String SQL = "SELECT * FROM visited WHERE id = ?";

		Visited visited = jdbcTemplateObject.queryForObject(SQL, new Object[] { id }, new VisitedMapper());
		return visited;
	}

	@Override
	public List<Visited> listVisited(String userid) {
		List<Visited> visited;
		if (userid == null)  {
			String SQL = "SELECT * FROM visited";
			
			visited = jdbcTemplateObject.query(SQL, new VisitedMapper());
		} else {
			String SQL = "SELECT * FROM visited WHERE userid = ?";

			visited = jdbcTemplateObject.query(SQL, new VisitedMapper(), userid);
		}
		return visited;
	}

	@Override
	public void delete(Integer id) {
		String SQL = "DELETE FROM visited WHERE id = ?";

		jdbcTemplateObject.update(SQL, id);
		return;
	}

	@Override
	public void update(Integer id, String userid, Integer markerid) {
		String SQL = "UPDATE visited SET userid = ?, markerid = ? WHERE id = ?";

		jdbcTemplateObject.update(SQL, userid, markerid, id);
		return;
	}

}
