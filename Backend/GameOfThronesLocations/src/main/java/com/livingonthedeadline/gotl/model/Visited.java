package com.livingonthedeadline.gotl.model;

public class Visited {
	private Integer id;
	private String userid;
	private Integer markerid;
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getUserid() {
		return userid;
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public Integer getMarkerid() {
		return markerid;
	}
	
	public void setMarkerid(Integer markerid) {
		this.markerid = markerid;
	}
}
