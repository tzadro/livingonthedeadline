package com.livingonthedeadline.gotl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.livingonthedeadline.gotl.model.Visited;

public class VisitedMapper implements RowMapper<Visited> {
	public Visited mapRow(ResultSet rs, int rowNum) throws SQLException {
		Visited visited = new Visited();
		visited.setId(rs.getInt("id"));
		visited.setUserid(rs.getString("userid"));
		visited.setMarkerid(rs.getInt("markerid"));
		return visited;
	}
}