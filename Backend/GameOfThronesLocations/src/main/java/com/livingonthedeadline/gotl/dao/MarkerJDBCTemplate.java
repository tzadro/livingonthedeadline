package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import com.livingonthedeadline.gotl.mapper.MarkerMapper;
import com.livingonthedeadline.gotl.model.Marker;

public class MarkerJDBCTemplate implements MarkerDAO {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;

	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
	}

	@Override
	public void create(String name, String description, Double lat, Double lng, Integer cityid, Integer typeid) {
		String SQL = "INSERT INTO marker (name, description, lat, lng, picurl, cityid) values (?, ?, ?, ?, ?, ?)";

		jdbcTemplateObject.update(SQL, name, description, lat, lng, cityid, typeid);
		return;
	}

	@Override
	public Marker getMarker(Integer id) {
		String SQL = "SELECT * FROM marker WHERE id = ?";

		Marker marker = jdbcTemplateObject.queryForObject(SQL, new Object[] { id }, new MarkerMapper());
		return marker;
	}

	@Override
	public List<Marker> listMarkers(Integer cityid) {
		List<Marker> markers;
		if (cityid == null)  {
			String SQL = "SELECT * FROM marker";

			markers = jdbcTemplateObject.query(SQL, new MarkerMapper());
		} else {
			String SQL = "SELECT * FROM marker WHERE cityid = ?";

			markers = jdbcTemplateObject.query(SQL, new MarkerMapper(), cityid);
		}
		return markers;
	}
	
	@Override
	public List<Marker> listNotVisitedByUser(Integer cityid, String userid) {
		List<Marker> markers;
		if (cityid == null)  {
			String SQL = "SELECT * FROM marker AS outter WHERE typeid = 1 AND NOT EXISTS (SELECT * FROM visited WHERE outter.id = markerid AND userid = ?)";

			markers = jdbcTemplateObject.query(SQL, new MarkerMapper(), userid);
		} else {
			String SQL = "SELECT * FROM marker AS outter WHERE cityid = ? AND typeid = 1 AND NOT EXISTS (SELECT * FROM visited WHERE outter.id = markerid AND userid = ?)";

			markers = jdbcTemplateObject.query(SQL, new MarkerMapper(), cityid, userid);
		}
		return markers;
	}
	
	@Override
	public void delete(Integer id) {
		String SQL = "DELETE FROM marker WHERE id = ?";

		jdbcTemplateObject.update(SQL, id);
		return;
	}

	@Override
	public void update(Integer id, String name, String description, Double lat, Double lng, Integer cityid, Integer typeid) {
		String SQL = "UPDATE marker SET name = ?, description = ?, lat = ?, lng = ?, cityid = ?, typeid = ? WHERE id = ?";

		jdbcTemplateObject.update(SQL, name, description, lat, lng, cityid, typeid, id);
		return;
	}
}
