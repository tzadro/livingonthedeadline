package com.livingonthedeadline.gotl.controller;

import java.util.List;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.livingonthedeadline.gotl.dao.MarkerDAO;
import com.livingonthedeadline.gotl.model.Marker;

@RestController
public class MarkerController {
	private MarkerDAO markerDAO = (MarkerDAO) new ClassPathXmlApplicationContext("beans.xml").getBean("markerDAO");

	@RequestMapping("marker/create")
	public void create(@RequestParam(value = "name") String name,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "lat") Double lat, @RequestParam(value = "lng") Double lng,
			@RequestParam(value = "cityid", required = false) Integer cityid,
			@RequestParam(value = "typeid") Integer typeid) {
		markerDAO.create(name, description, lat, lng, cityid, typeid);
	}

	@RequestMapping("marker/getMarker")
	public Marker getMarker(@RequestParam(value = "id") Integer id) {
		return markerDAO.getMarker(id);
	}

	@RequestMapping("marker/listMarkers")
	public List<Marker> listMarkers(@RequestParam(value = "cityid", required = false) Integer cityid) {
		return markerDAO.listMarkers(cityid);
	}
	
	@RequestMapping("marker/listNotVisitedByUser")
	public List<Marker> listNotVisitedByUser(@RequestParam(value = "cityid", required = false) Integer cityid,
			@RequestParam(value = "userid") String userid) {
		return markerDAO.listNotVisitedByUser(cityid, userid);
	}

	@RequestMapping("marker/delete")
	public void delete(@RequestParam(value = "id") Integer id) {
		markerDAO.delete(id);
	}

	@RequestMapping("marker/update")
	public void update(@RequestParam(value = "id") Integer id, @RequestParam(value = "name") String name,
			@RequestParam(value = "description", required = false) String description,
			@RequestParam(value = "lat") Double lat, @RequestParam(value = "lng") Double lng,
			@RequestParam(value = "cityid") Integer cityid,
			@RequestParam(value = "typeid") Integer typeid) {
		markerDAO.update(id, name, description, lat, lng, cityid, typeid);
	}
}
