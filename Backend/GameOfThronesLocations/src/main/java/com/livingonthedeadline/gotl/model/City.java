package com.livingonthedeadline.gotl.model;

public class City {
	private Integer id;
	private String name;
	private Double swlat;
	private Double swlng;
	private Double nelat;
	private Double nelng;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getSwlat() {
		return swlat;
	}

	public void setSwlat(Double swlat) {
		this.swlat = swlat;
	}

	public Double getSwlng() {
		return swlng;
	}

	public void setSwlng(Double swlng) {
		this.swlng = swlng;
	}

	public Double getNelat() {
		return nelat;
	}

	public void setNelat(Double nelat) {
		this.nelat = nelat;
	}

	public Double getNelng() {
		return nelng;
	}

	public void setNelng(Double nelng) {
		this.nelng = nelng;
	}
}
