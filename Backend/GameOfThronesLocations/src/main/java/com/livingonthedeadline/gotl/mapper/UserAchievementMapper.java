package com.livingonthedeadline.gotl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.livingonthedeadline.gotl.model.UserAchievement;

public class UserAchievementMapper implements RowMapper<UserAchievement> {
	public UserAchievement mapRow(ResultSet rs, int rowNum) throws SQLException {
		UserAchievement userAchievement = new UserAchievement();
		userAchievement.setId(rs.getInt("id"));
		userAchievement.setName(rs.getString("name"));
		userAchievement.setDescription(rs.getString("description"));
		userAchievement.setUserHas(new Boolean(rs.getInt("userHas") == 1));
		return userAchievement;
	}
}