package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import com.livingonthedeadline.gotl.model.Visited;

public interface VisitedDAO {
	/**
	 * This is the method to be used to initialize database resources ie.
	 * connection.
	 */
	public void setDataSource(DataSource dataSource);

	/**
	 * This is the method to be used to create a record in the visited table.
	 */
	public void create(String userid, Integer markerid);

	/**
	 * This is the method to be used to list down a record from the visited
	 * table corresponding to a passed visited id.
	 */
	public Visited getVisited(Integer id);

	/**
	 * This is the method to be used to list down all the records from the
	 * visited table from specified visited, if null then whole table.
	 */
	public List<Visited> listVisited(String userid);

	/**
	 * This is the method to be used to delete a record from the visited table
	 * corresponding to a passed visited id.
	 */
	public void delete(Integer id);

	/**
	 * This is the method to be used to update a record into the visited table.
	 */
	public void update(Integer id, String userid, Integer markerid);
}