package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import com.livingonthedeadline.gotl.model.Achievement;
import com.livingonthedeadline.gotl.model.UserAchievement;

public interface AchievementDAO {
	/**
	 * This is the method to be used to initialize database resources ie.
	 * connection.
	 */
	public void setDataSource(DataSource dataSource);

	/**
	 * This is the method to be used to create a record in the Achievement table.
	 */
	public void create(String name, String description);

	/**
	 * This is the method to be used to list down a record from the Achievement
	 * table corresponding to a passed achievement id.
	 */
	public Achievement getAchievement(Integer id);

	/**
	 * This is the method to be used to list down all the records from the
	 * Achievement table from specified city, if null then whole table.
	 */
	public List<UserAchievement> listUserAchievements(String userid);

	/**
	 * This is the method to be used to delete a record from the Achievement table
	 * corresponding to a passed achievement id.
	 */
	public void delete(Integer id);

	/**
	 * This is the method to be used to update a record into the Achievement table.
	 */
	public void update(Integer id, String name, String description);
}