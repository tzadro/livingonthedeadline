package com.livingonthedeadline.gotl.model;

public class UserAchievement {
	private Integer id;
	private String name;
	private String description;
	private Boolean userHas;
	
	public UserAchievement() {
		super();
	}
	
	public UserAchievement(Achievement a, Boolean userHas) {
		this.id = a.getId();
		this.name = a.getName();
		this.description = a.getDescription();
		this.userHas = userHas;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Boolean getUserHas() {
		return userHas;
	}
	
	public void setUserHas(Boolean userHas) {
		this.userHas = userHas;
	}
}
