package com.livingonthedeadline.gotl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.livingonthedeadline.gotl.model.City;

public class CityMapper implements RowMapper<City> {
	public City mapRow(ResultSet rs, int rowNum) throws SQLException {
		City city = new City();
		city.setId(rs.getInt("id"));
		city.setName(rs.getString("name"));
		city.setSwlat(rs.getDouble("swlat"));
		city.setSwlng(rs.getDouble("swlng"));
		city.setNelat(rs.getDouble("nelat"));
		city.setNelng(rs.getDouble("nelng"));
		return city;
	}
}