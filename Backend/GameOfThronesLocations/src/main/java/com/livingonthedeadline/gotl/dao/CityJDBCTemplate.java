package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import com.livingonthedeadline.gotl.mapper.CityMapper;
import com.livingonthedeadline.gotl.model.City;

public class CityJDBCTemplate implements CityDAO {
	private DataSource dataSource;
	private JdbcTemplate jdbcTemplateObject;
	
	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplateObject = new JdbcTemplate(this.dataSource);
	}

	@Override
	public void create(String name, Double swlat, Double swlng, Double nelat, Double nelng) {
		String SQL = "INSERT INTO city (name, swlat, swlng, nelat, nelng) values (?, ?, ?, ?, ?)";

		jdbcTemplateObject.update(SQL, name, swlat, swlng, nelat, nelng);
		return;
	}

	@Override
	public City getCity(Integer id) {
		String SQL = "SELECT * FROM city WHERE id = ?";

		City city = jdbcTemplateObject.queryForObject(SQL, new Object[] { id }, new CityMapper());
		return city;
	}

	@Override
	public List<City> listCities() {
		String SQL = "SELECT * FROM city";
		
		List<City> cities = jdbcTemplateObject.query(SQL, new CityMapper());
		return cities;
	}

	@Override
	public void delete(Integer id) {
		String SQL = "DELETE FROM city WHERE id = ?";

		jdbcTemplateObject.update(SQL, id);
		return;
	}

	@Override
	public void update(Integer id, String name, Double swlat, Double swlng, Double nelat, Double nelng) {
		String SQL = "UPDATE city SET name = ?, swlat = ?, swlng = ?, nelat = ?, nelng = ? WHERE id = ?";

		jdbcTemplateObject.update(SQL, name, swlat, swlng, nelat, nelng, id);
		return;
	}

}
