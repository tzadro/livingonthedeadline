package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import com.livingonthedeadline.gotl.model.City;

public interface CityDAO {
	/**
	 * This is the method to be used to initialize database resources ie.
	 * connection.
	 */
	public void setDataSource(DataSource dataSource);

	/**
	 * This is the method to be used to create a record in the Marker table.
	 */
	public void create(String name, Double swlat, Double swlng, Double nelat, Double nelng);

	/**
	 * This is the method to be used to list down a record from the Marker
	 * table corresponding to a passed marker id.
	 */
	public City getCity(Integer id);

	/**
	 * This is the method to be used to list down all the records from the
	 * Marker table from specified city, if null then whole table.
	 */
	public List<City> listCities();

	/**
	 * This is the method to be used to delete a record from the Marker table
	 * corresponding to a passed marker id.
	 */
	public void delete(Integer id);

	/**
	 * This is the method to be used to update a record into the Marker table.
	 */
	public void update(Integer id, String name, Double swlat, Double swlng, Double nelat, Double nelng);
}