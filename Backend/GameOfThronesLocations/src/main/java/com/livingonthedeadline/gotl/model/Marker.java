package com.livingonthedeadline.gotl.model;

public class Marker {
	private Integer id;
	private String name;
	private String description;
	private Double lat;
	private Double lng;
	private Integer typeid;
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setLat(Double lat) {
		this.lat = lat;
	}
	
	public Double getLat() {
		return lat;
	}
	
	public void setLng(Double lng) {
		this.lng = lng;
	}
	
	public Double getLng() {
		return lng;
	}
	
	public void setTypeid(Integer typeid) {
		this.typeid = typeid;
	}
	
	public Integer getTypeid() {
		return typeid;
	}
}
