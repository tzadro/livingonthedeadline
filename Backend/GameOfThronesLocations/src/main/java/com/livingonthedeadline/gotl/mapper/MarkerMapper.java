package com.livingonthedeadline.gotl.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import com.livingonthedeadline.gotl.model.Marker;

public class MarkerMapper implements RowMapper<Marker> {
	public Marker mapRow(ResultSet rs, int rowNum) throws SQLException {
		Marker marker = new Marker();
		marker.setId(rs.getInt("id"));
		marker.setName(rs.getString("name"));
		marker.setDescription(rs.getString("description"));
		marker.setLat(rs.getDouble("lat"));
		marker.setLng(rs.getDouble("lng"));
		marker.setTypeid(rs.getInt("typeid"));
		return marker;
	}
}