package com.livingonthedeadline.gotl.dao;

import java.util.List;
import javax.sql.DataSource;
import com.livingonthedeadline.gotl.model.User;

public interface UserDAO {
	/**
	 * This is the method to be used to initialize database resources ie.
	 * connection.
	 */
	public void setDataSource(DataSource dataSource);

	/**
	 * This is the method to be used to create a record in the User table.
	 */
	public void create(String id);

	/**
	 * This is the method to be used to list down a record from the User
	 * table corresponding to a passed user id.
	 */
	public boolean getUser(String id);

	/**
	 * This is the method to be used to list down all the records from the
	 * User table.
	 */
	public List<User> listUsers();

	/**
	 * This is the method to be used to delete a record from the User table
	 * corresponding to a passed user id.
	 */
	public void delete(String id);
}